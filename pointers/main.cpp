#include <iostream>

void smartPointers()
{

}

int main(int, char**)
{
    void* ptr = 0; // void means that the pointer is typeless
    // 0 is not a valid memory address (not a valid pointer)
    ptr = NULL;    // C define
    ptr = nullptr; // keyword introduced in C++11

    int var = 8;
    void* varPtr = &var; // & takes the memory address of a variable
    int* intPtr = &var;
    double* dblPtr = (double*)&var;

    // all 3 are going to print the same address:
    std::cout << varPtr << "\n";
    std::cout << intPtr << "\n";
    std::cout << dblPtr << "\n";

    // * is dereferencing (get access to the data from pointer)
    // *varPtr = 10; // the compiler doesn't know how many bytes to write:
                     // is 10 a byte, short, int, long?
    *intPtr = 10;

    std::cout << var << "\n"; // output the value of the original variable: 10

    // create a variable on the heap
    char* buffer = new char[8]; // allocated 8 bytes of memory and return a pointer
                                // to the beginning of that block
    memset(buffer, 0, 8);       // fill a block of memory (pointer, value, number of bytes)

    char** charPtr = &buffer;

    delete[] buffer;            // deallocated the buffer
}